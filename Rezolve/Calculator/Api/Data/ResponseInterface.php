<?php

namespace Rezolve\Calculator\Api\Data;

/**
 * Interface ResultInterface
 * @package Rezolve\Calculator\Api\Data
 */
interface ResponseInterface
{
    /**
     * @return string
     */
    public function getStatus(): string;

    /**
     * @param string $status
     * @return void
     */
    public function setStatus(string $status);

    /**
     * @return string
     */
    public function getResult(): string;

    /**
     * @param string $result
     * @return void
     */
    public function setResult(string $result);
}
