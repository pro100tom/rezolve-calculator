<?php

namespace Rezolve\Calculator\Api;

use Rezolve\Calculator\Api\Data\ResponseInterface;

/**
 * Interface RequestHandlerInterface
 * @package Rezolve\Calculator\Api\Calculator
 */
interface RequestHandlerInterface
{
    /**
     * @return \Rezolve\Calculator\Api\Data\ResponseInterface
     * @throws \Magento\Framework\Webapi\Exception
     */
    public function getResponse() : ResponseInterface;
}
