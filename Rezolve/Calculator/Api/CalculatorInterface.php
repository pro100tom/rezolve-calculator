<?php

namespace Rezolve\Calculator\Api;

/**
 * Interface CalculatorInterface
 * @package Rezolve\Calculator\Api
 */
interface CalculatorInterface
{
    /**
     * @param float $left
     * @param float $right
     * @param string $operator
     * @param int $precision
     * @return float
     * @throws \InvalidArgumentException
     * @throws \Zend\Math\BigInteger\Exception\DivisionByZeroException
     */
    public function calculate(float $left, float $right, string $operator, int $precision = 2) : float;
}
