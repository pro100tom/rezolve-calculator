# Rezolve Calculator

### Description
This module provides the functionality to perform calculations using the REST API.

### Installation
To test the functionality please install the module by pasting it into the **app/code/** directory of the project.
Run **setup/upgrade**. Make sure the Magento 2.1.11 instance is installed and configured.

##### Running
To run the API calls use any API tool of your choice (Postman for instance).
Choose POST from the request type dropdown and paste **http://[your-domain-url]/index.php/rest/default/V1/api/rce/calculator** url.
Under **body** section paste three key/value pairs (left, right, operator) with the values of your choice.
Parameters **left** and **right** are of type float. Parameter **operator** is a string (valid values: **add**, **subtract**, **multiply**, **divide**, **power**).
Click on **send** button to send the request and see the results.

##### Testing
Before running the tests make sure you have the MEQP2 installed and configured. Also make sure all the xml and sql files are configured in your project as well.
To run the tests navigate (in terminal) to the **dev/tests/api-functional** and run **../../../vendor/bin/phpunit ../../../app/code/Rezolve/Calculator/Test/Api/RequestHandlerInterfaceTest.php** command.

### Known Bugs
N/A

***

Author - Tomas Baranauskas (tomas@tomasbaranauskas.com)
