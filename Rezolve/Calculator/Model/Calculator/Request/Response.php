<?php

namespace Rezolve\Calculator\Model\Calculator\Request;

use Rezolve\Calculator\Api\Data\ResponseInterface;

/**
 * Class Response
 * @package Rezolve\Calculator\Model\Calculator\Request
 */
class Response implements ResponseInterface
{
    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $result;

    /**
     * @inheritDoc
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @inheritDoc
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @inheritDoc
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @inheritDoc
     */
    public function setResult(string $result)
    {
        $this->result = $result;
    }
}
