<?php

namespace Rezolve\Calculator\Test\Api;

use Magento\Framework\Webapi\Request;
use Magento\TestFramework\TestCase\WebapiAbstract;

/**
 * Class RequestHandlerInterfaceTest
 * @package Rezolve\Calculator\Test\Api
 */
class RequestHandlerInterfaceTest extends WebapiAbstract
{
    const RESOURCE_PATH = '/V1/api/rce/calculator';

    /**
     *
     */
    public function testGetResponseAddSuccess()
    {
        $serviceInfo = $this->buildServiceInfo([
            'left' => 4,
            'right' => 3,
            'operator' => 'add',
        ]);

        $response = $this->_webApiCall($serviceInfo);
        $expected = "7.00";
        $actual = (string)$response['result'];
        $this->assertTrue($expected === $actual, "Expected: $expected, actual: $actual");
    }

    /**
     *
     */
    public function testGetResponseSubtractSuccess()
    {
        $serviceInfo = $this->buildServiceInfo([
            'left' => 4,
            'right' => 3,
            'operator' => 'subtract',
        ]);

        $response = $this->_webApiCall($serviceInfo);
        $expected = "1.00";
        $actual = (string)$response['result'];
        $this->assertTrue($expected === $actual, "Expected: $expected, actual: $actual");
    }

    /**
     *
     */
    public function testGetResponseMultiplySuccess()
    {
        $serviceInfo = $this->buildServiceInfo([
            'left' => 4,
            'right' => 3,
            'operator' => 'multiply',
        ]);

        $response = $this->_webApiCall($serviceInfo);
        $expected = "12.00";
        $actual = (string)$response['result'];
        $this->assertTrue($expected === $actual, "Expected: $expected, actual: $actual");
    }

    /**
     *
     */
    public function testGetResponseDivideSuccess()
    {
        $serviceInfo = $this->buildServiceInfo([
            'left' => 4,
            'right' => 3,
            'operator' => 'divide',
        ]);

        $response = $this->_webApiCall($serviceInfo);
        $expected = "1.33";
        $actual = (string)$response['result'];
        $this->assertTrue($expected === $actual, "Expected: $expected, actual: $actual");
    }

    /**
     *
     */
    public function testGetResponsePowerSuccess()
    {
        $serviceInfo = $this->buildServiceInfo([
            'left' => 4,
            'right' => 3,
            'operator' => 'power',
        ]);

        $response = $this->_webApiCall($serviceInfo);
        $expected = "64.00";
        $actual = (string)$response['result'];
        $this->assertTrue($expected === $actual, "Expected: $expected, actual: $actual");
    }

    /**
     *
     */
    public function testGetResponsePrecision4()
    {
        $serviceInfo = $this->buildServiceInfo([
            'left' => 11,
            'right' => 7,
            'operator' => 'divide',
            'precision' => 4,
        ]);

        $response = $this->_webApiCall($serviceInfo);
        $expected = "1.5714";
        $actual = (string)$response['result'];
        $this->assertTrue($expected === $actual, "Expected: $expected, actual: $actual");
    }

    /**
     *
     */
    public function testGetResponsePrecision0()
    {
        $serviceInfo = $this->buildServiceInfo([
            'left' => 11,
            'right' => 7,
            'operator' => 'divide',
            'precision' => 0,
        ]);

        $response = $this->_webApiCall($serviceInfo);
        $expected = "2";
        $actual = (string)$response['result'];
        $this->assertTrue($expected === $actual, "Expected: $expected, actual: $actual");
    }

    /**
     *
     */
    public function testGetResponsePrecisionNegative()
    {
        $serviceInfo = $this->buildServiceInfo([
            'left' => 1100,
            'right' => 7,
            'operator' => 'divide',
            'precision' => -2,
        ]);

        $response = $this->_webApiCall($serviceInfo);
        $expected = "200";
        $actual = (string)$response['result'];
        $this->assertTrue($expected === $actual, "Expected: $expected, actual: $actual");
    }

    /**
     * @expectedException \Exception
     */
    public function testGetResponseRequiredArgumentMissing()
    {
        $serviceInfo = $this->buildServiceInfo([
            'right' => 7,
            'operator' => 'divide',
            'precision' => -2,
        ]);

        $this->_webApiCall($serviceInfo);
    }

    /**
     * @expectedException \Exception
     */
    public function testGetResponseDivisionByZero()
    {
        $serviceInfo = $this->buildServiceInfo([
            'left' => 4,
            'right' => 0,
            'operator' => 'divide',
        ]);

        $this->_webApiCall($serviceInfo);
    }

    /**
     * @expectedException \Exception
     */
    public function testGetResponseInvalidOperator()
    {
        $serviceInfo = $this->buildServiceInfo([
            'left' => 4,
            'right' => 5,
            'operator' => 'invalid',
        ]);

        $this->_webApiCall($serviceInfo);
    }

    /**
     * @param array $arguments
     * @return array[]
     */
    protected function buildServiceInfo(array $arguments) : array
    {
        return [
            'rest' => [
                'resourcePath' => implode('?', [
                    self::RESOURCE_PATH,
                    $this->buildQuery($arguments),
                ]),
                'httpMethod' => Request::METHOD_POST,
            ]
        ];
    }

    /**
     * @param array $arguments
     * @return string
     */
    protected function buildQuery(array $arguments) : string
    {
        return http_build_query($arguments);
    }
}
