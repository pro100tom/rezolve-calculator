<?php

namespace Rezolve\Calculator\Service;

use InvalidArgumentException;
use Rezolve\Calculator\Api\CalculatorInterface;
use Zend\Math\BigInteger\Exception\DivisionByZeroException;

/**
 * Class Calculator
 * @package Rezolve\Calculator\Service
 */
class Calculator implements CalculatorInterface
{
    /**
     * @inheritDoc
     */
    public function calculate(float $left, float $right, string $operator, int $precision = 2) : float
    {
        switch ($operator) {
            case 'add':
                $result = $this->add($left, $right);
                break;
            case 'subtract':
                $result = $this->subtract($left, $right);
                break;
            case 'multiply':
                $result = $this->multiply($left, $right);
                break;
            case 'divide':
                $result = $this->divide($left, $right);
                break;
            case 'power':
                $result = $this->power($left, $right);
                break;
            default:
                throw new InvalidArgumentException(__("Operator value %1 is invalid", $operator));
        }

        return round($result, $precision);
    }

    /**
     * @param float $x
     * @param float $y
     * @return float
     */
    protected function add(float $x, float $y) : float
    {
        return $x + $y;
    }

    /**
     * @param float $x
     * @param float $y
     * @return float
     */
    protected function subtract(float $x, float $y) : float
    {
        return $x - $y;
    }

    /**
     * @param float $x
     * @param float $y
     * @return float
     */
    protected function multiply(float $x, float $y) : float
    {
        return $x * $y;
    }

    /**
     * @param float $x
     * @param float $y
     * @return float
     * @throws DivisionByZeroException
     */
    protected function divide(float $x, float $y) : float
    {
        if ($y == 0) {
            throw new DivisionByZeroException(__('Division by zero is not allowed'));
        }

        return $x / $y;
    }

    /**
     * @param float $x
     * @param float $y
     * @return float
     */
    protected function power(float $x, float $y) : float
    {
        return pow($x, $y);
    }
}
