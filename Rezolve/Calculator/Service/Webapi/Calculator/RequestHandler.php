<?php

namespace Rezolve\Calculator\Service\Webapi\Calculator;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Webapi\Exception as WebapiException;
use Rezolve\Calculator\Api\CalculatorInterface;
use Rezolve\Calculator\Api\Data\ResponseInterface;
use Rezolve\Calculator\Api\Data\ResponseInterfaceFactory;
use Rezolve\Calculator\Api\RequestHandlerInterface;
use Throwable;

/**
 * Class RequestHandler
 * @package Rezolve\Calculator\Service\Calculator
 */
class RequestHandler implements RequestHandlerInterface
{
    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ResponseInterfaceFactory
     */
    protected $responseFactory;

    /**
     * @var CalculatorInterface
     */
    protected $calculator;

    /**
     * RequestHandler constructor.
     * @param RequestInterface $request
     * @param ResponseInterfaceFactory $responseFactory
     * @param CalculatorInterface $calculator
     */
    public function __construct(
        RequestInterface $request,
        ResponseInterfaceFactory $responseFactory,
        CalculatorInterface $calculator
    ) {
        $this->request = $request;
        $this->responseFactory = $responseFactory;
        $this->calculator = $calculator;
    }

    /**
     * @inheritDoc
     */
    public function getResponse() : ResponseInterface
    {
        $left = $this->request->getParam('left');
        $right = $this->request->getParam('right');
        $operator = $this->request->getParam('operator');

        foreach (['left' => $left, 'right' => $right, 'operator' => $operator] as $key => $param) {
            if ($param === null) {
                throw new WebapiException(__("Mandatory parameter %1 is missing from the request", $key));
            }
        }

        $precision = $this->request->getParam('precision');
        if ($precision === null) {
            $precision = 2;
        }

        $response = $this->responseFactory->create();
        try {
            $response->setResult(
                number_format(
                    $this->calculator->calculate($left, $right, $operator, $precision),
                    $precision
                )
            );
            $response->setStatus('OK');
        } catch (Throwable $e) {
            throw new WebapiException(__($e->getMessage()));
        }

        return $response;
    }
}
